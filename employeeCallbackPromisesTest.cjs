const data = require('./data.json');
const fs = require('fs');
const {detailsOfEmployees, groupEmployeesOnCompany, detailsOfACompany, removeId, sortEmployees, swapCompanies, addBirthdays} = require('./employeeCallbackPromisesSolution.cjs');
let employeeData = data.employees;
let outputPath = 'output/'
let dataIds = [2,13,23];

function employeeCallbacksPromises(employeeData){
    detailsOfEmployees(employeeData,dataIds).then(result=>{
        return groupEmployeesOnCompany(result)
    }).then((result)=>{
        return detailsOfACompany(result)
    }).then((result)=>{
        return removeId(result);
    }).then((result)=>{
        return sortEmployees(result);
    }).then((result)=>{
        return swapCompanies(result);
    }).then((result)=>{
        return addBirthdays(result);
    }).then((result)=>{
        console.log(result);
    }).catch((err)=>{
        console.error(err);
    })
}

employeeCallbacksPromises(employeeData);